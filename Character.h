
#ifndef CHARACTER_H
#define CHARACTER_H

#include "node.h"
/*
    Character.h
    The character class contains two integer variables that contains
    position X and Y of the agent. Several flags that show current agent actions.
    It contains two nodes to contain the previous node the agent was on and the current node
    its on.
    
*/
class Character
{
    
public: 
    //The variables should be private
    //Agents position expressed as x and y coordinates
	int posX,posY;
	bool hasBow,hasFired,hasGold,droppedBow,droppedGold;
	Node *prevNode,*currentNode;
    
    //Default constructor
	Character();
    // overloaded constructor
	Character(int ,int );
    
    //typical set functions for used variables
    void setPosX(int);
    void setPosY(int);
    void setPosition(ifstream &);  
    void setCurrentNode(Node*);
    void setPrevNode(Node*);
    
    //typical get functions to obtain values
    int getPosX();
    int getPosY();
    Node* getCurrentNode();
    Node* getPrevNode();
    
    //Function to shoot the bow
    bool shootBow();
    // Drop function for dropping bow or gold
    void drop();
    // Grab function for grabbing bow or ghold
    void grab();
};

/*
    Character 
    Default Constructor
    Input  :: None
    Output :: None
    Default constructor to initualize the character oject
*/
Character::Character()
{
    hasBow = true;
    hasGold = hasFired = droppedBow = droppedGold= false;
}
/*
    Character 
    Overloaded Constructor
    Input  :: Intgers x and y, corresponding to the character position
    Output :: None
    Default constructor to initualize the character oject
*/
Character::Character(int x,int y)
{
    hasBow = true;
    hasGold = hasFired = droppedBow = droppedGold= false;
    posX = x;
    posY = y;
}

// Set functions
void Character::setPosX(int x)
{   this->posX = x; }

void Character::setPosY(int y)
{   this->posY = y; }

void Character::setCurrentNode(Node *n)
{   this->currentNode = n; }
void Character::setPrevNode(Node *n)
{   this->prevNode = n  ; }
// Get functions
int Character::getPosX()
{   return posX;        }

int Character::getPosY()
{   return posY;        }

Node * Character::getCurrentNode()
{   return currentNode; }

Node * Character::getPrevNode()
{   return prevNode;    }

void Character::drop()
{
    if(hasBow)
    {
        hasBow = false;
        droppedBow = true;
        currentNode->hasBow = true;
    }
    else if( hasGold )
    {
        hasGold =false;
        droppedGold = true;
        currentNode->hasBow = true;
    }
    else
        ;
}
void Character::grab()
{
    if(currentNode->hasGold)
    {
        if(droppedBow)
        {
            hasGold = true;
             currentNode->hasGold = false;
                
         }
         
    }
    else if( currentNode->hasBow)
    {
        if(droppedGold)
        {
            hasBow = true;
            currentNode->hasBow = false;
        }
    }
   
}
bool Character::shootBow()
{
    //Check orientation
    
    //Check every node in that direction
    //If wumpus not in any of them
    //hit  wall, else
    //kill wumpus and scream everywheres
    //
}
#endif
