#include <iostream>
#include <string>
#include <ostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include "node.h"
#include "Map.h"
#include "Character.h"

//Dagoberto Rodriguez
//Alberto Garcia
//Assignment 1		Feb 1
/*
    Wumpus World
    Most of the classes have been split into their own Header and Implementation files.
    The node.h/node.cpp files describes the individual node and its directions
    The Map.h/map.cpp files connect all the nodes and is based off an adjency matrix
    The Character.h/Character.cpp file defines the agent and its attributes
    The Wumpus World class was left in main as we used it to work all our new code since
    we created header files for all the working code.
    
    The program begins in main by calling the Wumpus World object to run.
    The map contrusctor takes the config file and creates the initial map. 
    The wumpus world then works on the map to create the game.
    
*/

// for setting precepts on nodes
// can we set them as soon as that set precepts from the
// file are read only setting them for the nodes passed?
using namespace std;

//Wumpus World Class
class WumpusWorld : Map {
    
   Map m;
  bool running;
	string user_input;
    int score;
    vector<Node*> OpenList;
    vector<Node*> ClosedList;
    
public:
     
    WumpusWorld()
    {
        score = 0;
        running = true;
    	m = Map();
      
    }

    //Game starts here,  list of commands the game is programmed to take 
    // then calls the approppriate function that handles the rest.
    //Most functions aall void
	void run()
	{
            
           
		     
            A_Star();
            greedySearch();  
               
            m.printGrid();
	    	  
		    
		    
		  
	}

    //Removes player from previous node
     void removeOldPosition(int posX, int posY)
    {
        m.nodes[posY][posX].s[6] = "|             |\n" ;
    }
		
    

    // Rotates the player    
    void rotatePlayer(string direction)
    {
             
       for(int i = 0; i < m.n; i++)
	   {
			for(int j = 0; j < m.n; j++)
			{
				if(m.nodes[i][j].hasPlayer)
				{		
					m.setPlayerString(j,i, m.getOrientation());			
				}
			}	
	   }
	}
    
    
    //Pass a node. In each if statement in the next function
    //will call this function to get the Max(x2-x1,y2-y1) * 5
    //which will be the h.  Then in the next function we keep track 
    //of the cost by keeping the best option, its node coordinates or orientation
    // to move to the next node and repeat. So this might be doable recursively.
    // That should cover the majority of the greedy search. But this is my first
    //idea on how to implement this. 
    
int additional_cost(Node* node)
{
	int cost = 0; 
	//find current node.
    if(node)
    {
        cost += 5;
        if(node->hasWumpus)
            cost+=500;
        if(node->hasPit)
            cost +=500;
        if(node->stench)
            cost += 25;
        if(node->breeze)
            cost += 25;
    }

	return cost;

}

    int distance(Node* node)
    {
        //calculate 8 distance and return h
        //if(agent.getPrevNode().getPosX() )
        int x = abs(goldX - node->getPosX());
        int y = abs(goldY - node->getPosY());

        return (max(x,y) * 5);
       
    }
    
void greedySearch()
{
	
        int bestDistance = 2000;
        bool goldFound = false;
        Node* bestNode = NULL;
        Node* aCurrentNode = NULL;
        
	/*
  	*	Find the shortest path towards the goal.
  	*	Ignores pits/wumpus costs.
	*/
	
        while(!goldFound)
        {

            aCurrentNode = agent.currentNode;
            bestDistance = 2000;
            
      
            if(aCurrentNode->north)
            {
            
               if(bestDistance > distance(aCurrentNode->north))
                {   
                    bestNode = aCurrentNode->north; 
                    bestDistance = distance(aCurrentNode->north); 
                }
                
            }
            if(aCurrentNode->south)
            {   
                if(bestDistance > distance(aCurrentNode->south))
                { 
                    bestNode = aCurrentNode->south;           
                    bestDistance = distance(aCurrentNode->south);
                }
            }
            if(aCurrentNode->east)
            {   
                if(bestDistance > distance(aCurrentNode->east))
                {
                    bestNode = aCurrentNode->east; 
                    bestDistance = distance(aCurrentNode->east);
                }
            }

            if(aCurrentNode->west)
            {
                
                if(bestDistance > distance(aCurrentNode->west))
                {
                    bestNode = aCurrentNode->west;        
                    bestDistance = distance(aCurrentNode->west);
                }
            }
            
            if(aCurrentNode->nEast)
            {
                if(bestDistance > distance(aCurrentNode->nEast))
                {
                    bestNode = aCurrentNode->nEast;
                    bestDistance = distance(aCurrentNode->nEast);
                }
            }
            if(aCurrentNode->nWest)
            {
                if(bestDistance > distance(aCurrentNode->nWest))
                {
                    bestNode = aCurrentNode->nWest;
                    bestDistance = distance(aCurrentNode->nWest);
                }
            }
            if(aCurrentNode->sWest)
            {
                if(bestDistance > distance(aCurrentNode->sWest))
                {
                    bestNode = aCurrentNode->sWest;
                    bestDistance = distance(aCurrentNode->sWest);
                }
            }
            if(aCurrentNode->sEast)
            {
                if(bestDistance > distance(aCurrentNode->sEast))
                {
                    bestNode = aCurrentNode->sEast;
                    bestDistance = distance(aCurrentNode->sEast);
                }
            }
            
            if(bestNode->getPosX() == goldX && bestNode->getPosY()==goldY)
            {   cout << "Gold Found" << endl;
                goldFound = true;
            }

            if(bestNode)
            cout <<"Moved to: "<< bestNode->getPosX() << "," << bestNode->getPosY() << endl;
            
            if(aCurrentNode)
                aCurrentNode->visited = true;
            if(bestNode)
            {
                bestNode->visited = true;
                agent.setPrevNode(aCurrentNode);
                agent.setCurrentNode(bestNode);
                m.printSearch(bestNode->getPosY(),bestNode->getPosX(),"G");
                
            }
           
       }
	//m.printGrid();	
}
int getG(Node* nodeA, Node* nodeB)
{
    int x = abs(nodeA->getPosX() - nodeB->getPosX());
    int y = abs(nodeA->getPosY() - nodeB->getPosY());
    int g = max(x,y) + additional_cost(nodeB);
    return g;
}
 void populateOpenList(Node* aCurrentNode)
 {
     if(aCurrentNode->north)
            {
                if(!aCurrentNode->north->inOpenList)
                  {
                     
                     aCurrentNode->north->inOpenList = true;
                     OpenList.push_back(aCurrentNode->north);
                  }  
               
            }
            if(aCurrentNode->south)
            {   
            	if(!aCurrentNode->south->inOpenList)
                 {
                     
                     aCurrentNode->south->inOpenList = true;
                     OpenList.push_back(aCurrentNode->south);
                  }  
            
            }
            if(aCurrentNode->east)
            {   
            	if(!aCurrentNode->east->inOpenList)
                {
                     
                     aCurrentNode->east->inOpenList = true;
                     OpenList.push_back(aCurrentNode->east);
                }  
            }

            if(aCurrentNode->west)
            {
              if(!aCurrentNode->west->inOpenList)
                  {
               
                     aCurrentNode->west->inOpenList = true;
                      OpenList.push_back(aCurrentNode->west);
                  } 
            }
            
            if(aCurrentNode->nEast)
            {
            	if(!aCurrentNode->nEast->inOpenList)
                  {
                     
                     aCurrentNode->nEast->inOpenList = true;
                     OpenList.push_back(aCurrentNode->nEast);
                  }       
            }
            if(aCurrentNode->nWest)
            {
            	if(!aCurrentNode->nWest->inOpenList)
                   {
                    
                     aCurrentNode->nWest->inOpenList = true;
                      OpenList.push_back(aCurrentNode->nWest);
                  } 
            }
            if(aCurrentNode->sWest)
            {
                
                if(!aCurrentNode->sWest->inOpenList)
                    {
                     
                     aCurrentNode->sWest->inOpenList = true;
                     OpenList.push_back(aCurrentNode->sWest);
                  } 
            }
            if(aCurrentNode->sEast)
            {
            	if(!aCurrentNode->sEast->inOpenList)
                  {
                     aCurrentNode->sEast->inOpenList = true;
                     OpenList.push_back(aCurrentNode->sEast);
                  } 
            }
 }
     Node* checkPreviousNodes(Node* node)
    {
        if(node && node->parentNode)
        {
            cout << node->parentNode->getPosX() << "," << node->parentNode->getPosY() << endl;
            return checkPreviousNodes(node->parentNode);
        }
        
        return 0;
    }
 
 bool myfunction(Node* nI,Node* nJ)
 {
     return (nI->f  >  nJ->f);
 }
 vector<Node*> children;
 
 void populateChildList(Node* aCurrentNode)
 {
      if(aCurrentNode->north)
                    {                   
                            int nodesH = distance(aCurrentNode->north);
                            int nodesG = getG(aCurrentNode,aCurrentNode->north);
                            aCurrentNode->north->h = nodesH;
                            aCurrentNode->north->g = nodesG;
                            aCurrentNode->north->f = nodesH + nodesG;
                            aCurrentNode->north->parentNode = aCurrentNode;
                            children.push_back(aCurrentNode->north);
                            stringstream ss;
                            ss << aCurrentNode->north->f;
                            aCurrentNode->s[1] = "   " + ss.str() + "  " ;
                    }
                    if(aCurrentNode->south)
                    {   
                        
                            
                        int nodesG = getG(aCurrentNode,aCurrentNode->south);
                        int nodesH = distance(aCurrentNode->south);
                        aCurrentNode->south->h = nodesH;
                        aCurrentNode->south->g = nodesG;
                        aCurrentNode->south->f = nodesH + nodesG;
                        aCurrentNode->south->parentNode = aCurrentNode;
                        children.push_back(aCurrentNode->south);
                        
                        
                    }
                    if(aCurrentNode->east)
                    {   
                        
                            
                            int nodesG = getG(aCurrentNode,aCurrentNode->east);
                            int nodesH = distance(aCurrentNode->east);
                            aCurrentNode->east->h = nodesH;
                            
                            aCurrentNode->east->g = nodesG;
                            aCurrentNode->east->f = nodesH + nodesG;
                            aCurrentNode->east->parentNode = aCurrentNode;
                            children.push_back(aCurrentNode->east);
                
                        
                    }

                    if(aCurrentNode->west)
                    {
                    
                        
                            int nodesG = getG(aCurrentNode,aCurrentNode->west);
                            int nodesH = distance(aCurrentNode->west);
                            aCurrentNode->west->h = nodesH;
                            
                            aCurrentNode->west->g = nodesG;
                            aCurrentNode->west->f = nodesH + nodesG;
                            aCurrentNode->west->parentNode = aCurrentNode;
                            children.push_back(aCurrentNode->west);
                            
                        
                    }
                    
                    if(aCurrentNode->nEast)
                    {
                        
                            
                            int nodesH = distance(aCurrentNode->nEast);
                            aCurrentNode->nEast->h = nodesH;
                            int nodesG = getG(aCurrentNode,aCurrentNode->nEast);
                            aCurrentNode->nEast->g = nodesG;
                            aCurrentNode->nEast->f = nodesH + nodesG;
                            aCurrentNode->nEast->parentNode = aCurrentNode;
                            children.push_back(aCurrentNode->nEast);
                        
                                
                    }
                    if(aCurrentNode->nWest)
                    {
                        
                            
                    
                            int nodesH = distance(aCurrentNode->nWest);
                            aCurrentNode->nWest->h = nodesH;
                            int nodesG = getG(aCurrentNode,aCurrentNode->nWest);
                            aCurrentNode->nWest->g = nodesG;
                            aCurrentNode->nWest->f = nodesH + nodesG;
                            aCurrentNode->nWest->parentNode = aCurrentNode;
                            children.push_back(aCurrentNode->nWest);
                        
                        
                    }
                    if(aCurrentNode->sWest)
                    {
                        
                    
                
                            int nodesH = distance(aCurrentNode->sWest);
                            aCurrentNode->sWest->h = nodesH;
                            int nodesG = getG(aCurrentNode,aCurrentNode->sWest);
                            aCurrentNode->sWest->g = nodesG;
                            aCurrentNode->sWest->f = nodesH + nodesG;
                            aCurrentNode->sWest->parentNode = aCurrentNode;
                            children.push_back(aCurrentNode->sWest);
                
                        
                    }
                    if(aCurrentNode->sEast)
                    {
                        
                    
                            int nodesH = distance(aCurrentNode->sEast);
                            aCurrentNode->sEast->h = nodesH;
                            int nodesG = getG(aCurrentNode,aCurrentNode->sEast);
                            aCurrentNode->sEast->g = nodesG;
                            aCurrentNode->sEast->f = nodesH + nodesG;
                            aCurrentNode->sEast->parentNode = aCurrentNode;
                            children.push_back(aCurrentNode->sEast);
                    }
                
                
 }
void A_Star()
{

        
        bool goldFound = false;
        int i = 1;
  
        Node* aCurrentNode = NULL;
        Node* bestNode = NULL;
        
       
        OpenList.push_back(agent.currentNode);
        while( !OpenList.empty() && !goldFound )
        {
            aCurrentNode=OpenList.back();
             
            for(int iii=0;iii<OpenList.size();iii++)
            {
                if(OpenList[iii]->f < aCurrentNode->f)
                {
                    aCurrentNode = OpenList[iii]; 
                                                            
                }
            }
      
            
            populateChildList(aCurrentNode);
        
    
            //Goal state
            for(int iii=0;iii<children.size();iii++)
            {   
                //cout << iii << endl;
                if(children[iii]->getPosX() == goldX && children[iii]->getPosY()==goldY)
                {   
                    cout << "Gold Found" << endl;
                    goldFound = true;
                    bestNode = children[iii];
                }          
            }
            
            for(int iii=0;iii<OpenList.size();iii++)
            for(int jjj=0;jjj<children.size();jjj++)
            {   
                if(children[jjj]->getPosX() == OpenList[iii]->getPosX() && children[jjj]->getPosY() == OpenList[iii]->getPosY() && (children[jjj]->f > OpenList[iii]->f)  )
                { 
                    children[jjj]->inOpenList = true;   
                }  
            
            }
        
            for(int iii=0;iii<ClosedList.size();iii++)
            for(int jjj=0;jjj<children.size();jjj++)
            {   
                if(children[jjj]->getPosX() == ClosedList[iii]->getPosX() && children[jjj]->getPosY() == ClosedList[iii]->getPosY()&& (children[jjj]->f > OpenList[iii]->f)  )
                { 
                     children[jjj]->inClosedList = true;       
                }  
            
            }
        
            for(int jjj=0;jjj<children.size();jjj++)
                if(!children[jjj]->inOpenList && !children[jjj]->inClosedList)
                     OpenList.push_back(children[jjj]);
            
                 cout << aCurrentNode->getPosX() << " " << aCurrentNode->getPosY() << endl;
                 m.printSearch( aCurrentNode->getPosY(),aCurrentNode->getPosX(),  "A");
                 
                 
                 eraseFromOpenList(aCurrentNode);
                 ClosedList.push_back(aCurrentNode);
                 aCurrentNode->inClosedList = true;
                   
                 
                 
                    
       }
      
      // m.printGrid();
    
    
 
}

    void eraseFromOpenList(Node* node)
    {
        for(int i=0;i< OpenList.size();i++)
        {
            if(OpenList[i]->posX == node->posX && OpenList[i]->posY == node->posY)
                OpenList.erase(OpenList.begin() + i);
        }
    }


};


int main()
{
    //Linux terminal code for clearing the entire screen
 
	WumpusWorld w;
	
    	w.run();
	
	
	
	return 0;

}



