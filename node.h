#ifndef NODE_H
#define NODE_H
 /*
    Node.h
    The node file describes the individual unit that makes up the map.
    The node contains information on all nodes its connected to through
    directionality node pointers. 
    They also have flags for any precept associated with it and a visited flag.
    They also contain a variable to hold the cost associated with movement to it. 
    The node's position is denoted by an integer x and y variable. 
    
    
 */

#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <iostream>


using namespace std;
class Node
{

	public:
        int f,g,h;
        bool inOpenList,inClosedList;
		bool visited;
        bool stench,breeze,hasGold,hasPit,
        hasWumpus,hasBow,hasPlayer, hasBumped;
		string s[8];
		Node* parentNode;
		Node* north;
		Node* south;
		Node* east;
		Node* west;
		Node* nWest;
		Node* nEast;
		Node* sWest;
		Node* sEast;
		int n ;
		int posX,posY;

		Node();
        int getPosX()
        {return posX; }
        int getPosY()
        { return posY; }
		void print(int x, int y);	
};

/*** 
    Node 
    Constructor
    Input:: No input, default constructor
    Output:: No output.
    Sets various flags to falseand initializes the string array used for building the grid. 
***/
Node::Node()
{
    g = f = h = 0;
    visited = inOpenList = inClosedList = false;
    stench = breeze = hasGold = hasPit = hasWumpus= hasBow = hasPlayer= hasBumped = false;
    n=0;
    s[0] =  	"+-------------+" ;
    s[1] =		"|             |" ;
    s[2] =		"|             |" ;
    s[3] =		"|             |" ;
    s[4] =		"|             |" ;
    s[5] =		"|             |" ;
    s[6] =		"|             |" ;
    s[7] =		"+-------------+" ;
    
}
/*** 
    Print 
    Input  :: int x, int y. Locations that corresponding to the terminal's output space
    Output :: No return values, prints to screen.
    
    Print function called to print out the box that represents each individual node
    Takes two Integer inputs to align the box to its appropriate location on the terminal
    -- buggy for lower resolutions, small windows;
***/



#endif
